/*
    Copyright (c) 2023 Nick Donovan

    This file is part of hecc.

    hecc is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
    License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
    version.

    hecc is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
    details.

    You should have received a copy of the GNU General Public License along with hash_em. If not, see
    <https://www.gnu.org/licenses/>.
 */

const BUFF_SIZE: usize = 512 * 1024 * 1024;
const DEFAULT_ALGORITHM: &str = "SHA256";

#[derive(Debug)]
pub struct Args {
    algorithm: String,
    path: String,
    out_path: Option<String>,
    buff_size: usize,
    terminate_on_fail: bool,
}

impl Default for Args {
    fn default() -> Self {
        Args {
            algorithm: String::new(),
            path: String::new(),
            out_path: None,
            buff_size: BUFF_SIZE,
            terminate_on_fail: false,
        }
    }
}

impl Args {
    // exe.exe <hash algo> <path> [options]
    pub fn build(mut arg_itr: impl Iterator<Item=String>) -> Result<Args, String> {
        let exe_file = arg_itr.next().unwrap(); // exe.exe
        let usage_string = format!("{} <hash_algo> <path> [options]", exe_file);

        // <hash_algo> todo() add error checking once hasher is complete
        let mut algorithm = match arg_itr.next() {
            Some(alg) => alg,
            None => return Err(format!("Missing hash algorithm.\n{usage_string}")),
        };

        // <path> todo() verify path via error checking
        let path = match arg_itr.next() {
            Some(alg) => alg,
            None => {
                let temp = algorithm;
                algorithm = String::from(DEFAULT_ALGORITHM);
                temp
            },
        };

        // Init new Args with built hashmap
        let mut args = Args::default();
        args.algorithm = algorithm;
        args.path = path;

        // Iterate through remainder
        if let Err(e) = Self::parse_optional_args(&mut arg_itr, &mut args) {
            return Err(e);
        }

        Ok(args)
    }

    fn parse_optional_args(arg_itr: &mut (impl Iterator<Item=String> + Sized), args: &mut Args) -> Result<(), String> {
        while let Some(arg) = arg_itr.next() {
            // First item should be an arg
            if !is_arg(&arg) {
                return Err(format!("Value missing an argument: {}", arg));
            }

            // Boolean args don't need a value
            match arg.as_str() {
                "-t" => {
                    args.terminate_on_fail = true;
                    continue;
                }
                _ => (),
            }

            // Get the value for the arg
            let value = match arg_itr.next() {
                Some(v) => v,
                None => return Err(format!("Value missing for argument: {}", arg)),
            };

            // The value shouldn't be an arg
            if is_arg(&value) {
                return Err(format!("Argument missing required value: {}", arg));
            }

            // Add the value to the arg
            match arg.as_str() {
                "-o" => args.out_path = Some(value),
                "-bs" => {
                    args.buff_size = match value.parse::<usize>() {
                        Ok(v) => v,
                        Err(..) => return Err(format!("Invalid buffer size specified: {}", value)),
                    };
                }
                _ => return Err(format!("Unknown argument: {arg}")),
            };
        }
        Ok(())
    }


    pub fn get_algorithm(&self) -> &String {
        &self.algorithm
    }

    pub fn get_path(&self) -> &String {
        &self.path
    }

    pub fn get_buff_size(&self) -> usize {
        self.buff_size
    }

    pub fn get_output_file(&self) -> &Option<String> {
        &self.out_path
    }

    pub fn get_terminate_early(&self) -> bool {
        self.terminate_on_fail
    }
}

fn is_arg(s: &String) -> bool {
    s.starts_with('-')
}


#[cfg(test)]
mod tests {
    use crate::args::{Args, BUFF_SIZE};

    #[test]
    fn build_test_no_optional_args() {
        // No optional args
        let a = vec![
            String::from("exe.exe"),
            String::from("sha1"),
            String::from("./tests")];

        let args = Args::build(a.into_iter()).unwrap();

        assert_eq!(args.path, "./tests");
        assert_eq!(args.algorithm, "sha1");

        // Defaults
        assert_eq!(args.buff_size, BUFF_SIZE);
        assert_eq!(args.out_path, None);
        assert!(!args.terminate_on_fail);
    }

    #[test]
    fn build_test_out_path_arg() {
        let a = vec![
            String::from("exe.exe"),
            String::from("sha1"),
            String::from("./tests"),
            String::from("-o"),
            String::from("output_path"),
        ];

        let args = Args::build(a.into_iter()).unwrap();
        assert_eq!(args.path, "./tests");
        assert_eq!(args.algorithm, "sha1");
        assert_eq!(args.out_path.unwrap(), "output_path");


        // Defaults
        assert_eq!(args.buff_size, BUFF_SIZE);
        assert!(!args.terminate_on_fail);
    }

    #[test]
    fn build_test_buff_size_arg() {
        let a = vec![
            String::from("exe.exe"),
            String::from("sha1"),
            String::from("./tests"),
            String::from("-bs"),
            String::from("128"),
        ];

        let args = Args::build(a.into_iter()).unwrap();
        assert_eq!(args.path, "./tests");
        assert_eq!(args.algorithm, "sha1");
        assert_eq!(args.buff_size, 128usize);

        assert!(!args.terminate_on_fail);
        assert_eq!(args.out_path, None);
    }

    #[test]
    fn build_test_terminate_arg() {
        let a = vec![
            String::from("exe.exe"),
            String::from("sha1"),
            String::from("./tests"),
            String::from("-t"),
        ];

        let args = Args::build(a.into_iter()).unwrap();
        assert_eq!(args.path, "./tests");
        assert_eq!(args.algorithm, "sha1");
        assert!(args.terminate_on_fail);

        assert_eq!(args.buff_size, BUFF_SIZE);
        assert_eq!(args.out_path, None);
    }

    #[test]
    fn build_test_all_args() {
        let a = vec![
            String::from("exe.exe"),
            String::from("sha1"),
            String::from("./tests"),
            String::from("-t"),
            String::from("-bs"),
            String::from("128"),
            String::from("-o"),
            String::from("output_path"),
        ];

        let args = Args::build(a.into_iter()).unwrap();

        assert_eq!(args.path, "./tests");
        assert_eq!(args.algorithm, "sha1");
        assert!(args.terminate_on_fail);
        assert_eq!(args.buff_size, 128usize);
        assert_eq!(args.out_path.unwrap(), "output_path");
    }

    #[test]
    #[ignore]
    fn build_test_missing_algo() { // todo needs implementing when error checking above is added
        let a = vec![
            String::from("exe.exe"),
            String::from("./tests"),
            String::from("-o"),
            String::from("output_path"),
        ];
        Args::build(a.into_iter()).unwrap();
    }

    #[test]
    #[should_panic(expected = "missing required value: -bs")]
    fn build_test_missing_arg_value() {
        let a = vec![
            String::from("exe.exe"),
            String::from("sha1"),
            String::from("./tests"),
            String::from("-bs"),
            String::from("-o"),
            String::from("output_path"),
        ];

        Args::build(a.into_iter()).unwrap();
    }

    #[test]
    #[should_panic(expected = "missing an argument: panic")]
    fn build_test_missing_arg() {
        let a = vec![
            String::from("exe.exe"),
            String::from("sha1"),
            String::from("./tests"),
            String::from("panic"),
        ];
        Args::build(a.into_iter()).unwrap();
    }
}