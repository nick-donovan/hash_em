/*
    Copyright (c) 2023 Nick Donovan

    This file is part of hecc.

    hecc is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
    License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
    version.

    hecc is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
    details.

    You should have received a copy of the GNU General Public License along with hash_em. If not, see
    <https://www.gnu.org/licenses/>.
 */

use std::{fmt, io};

#[derive(Debug)]
pub struct HasherError(pub String);

impl fmt::Display for HasherError {
    fn fmt(&self, format: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(format, "{}", self.0)
    }
}

impl std::error::Error for HasherError {}

impl From<io::Error> for HasherError {
    fn from(error: io::Error) -> Self {
        HasherError(error.to_string())
    }
}

impl From<HasherError> for String {
    fn from(error: HasherError) -> Self { error.to_string() }
}

impl From<HasherError> for io::Error {
    fn from(error: HasherError) -> Self {
        io::Error::new(io::ErrorKind::Other, error.to_string())
    }
}

