/*
    Copyright (c) 2023 Nick Donovan

    This file is part of hecc.

    hecc is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
    License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
    version.

    hecc is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
    details.

    You should have received a copy of the GNU General Public License along with hash_em. If not, see
    <https://www.gnu.org/licenses/>.
 */

use std::fs;
use std::path::PathBuf;

/// Parses the provided path from the CLI and returns a vector of paths. If the path is a
/// directory, the directory is read and parsed.
/// # Examples
/// ```ignore
/// let dir_path = String::from("test_files/");
/// let paths = hash_em::paths::parse_path(&dir_path)?;
/// ```
pub fn parse_path(path: &String) -> Result<Vec<PathBuf>, String> {
    let mut paths = Vec::new();
    let path = PathBuf::from(path);
    if path.is_file() {
        paths.push(path)
    } else if path.is_dir() {
        paths = parse_dir(path)
    } else {
        return Err(format!("Could not find path: {}", path.display()));
    }

    Ok(paths)
}

fn parse_dir(dir_path: PathBuf) -> Vec<PathBuf> {
    let mut paths = Vec::new();
    let mut stack = Vec::new();

    stack.push(dir_path);

    while let Some(dir) = stack.pop() {
        let entries = match fs::read_dir(&dir) {
            Ok(e) => e,
            Err(e) => {
                eprintln!("{}: {}", dir.display(), e);
                continue;
            }
        };

        for entry in entries {
            match entry {
                Ok(e) => {
                    let path: PathBuf = e.path();
                    if path.is_file() {
                        paths.push(path);
                    } else if path.is_dir() {
                        stack.push(path);
                    }
                }
                Err(ref e) => {
                    eprintln!("{:?}: {}", entry, e);
                    continue;
                }
            }
        }
    }
    paths
}

#[cfg(test)]
mod tests {
    use std::path::PathBuf;
    use crate::paths::parse_path;

    #[test]
    fn test_parse_path_file() {
        let path = String::from("test_files/test.bin");
        let paths = parse_path(&path).unwrap();
        assert_eq!(paths.len(), 1);
        assert!(paths.contains(&PathBuf::from(path)))
    }

    #[test]
    fn test_parse_path_dir() {
        let path = String::from("test_files/");
        let paths = parse_path(&path).unwrap();
        assert_eq!(paths.len(), 5);
        assert!(paths.contains(&PathBuf::from("test_files/test.txt")));
        assert!(paths.contains(&PathBuf::from("test_files/50mb.bin")));
        assert!(paths.contains(&PathBuf::from("test_files/test.bin")));
        assert!(paths.contains(&PathBuf::from("test_files/test_dir/empty_file")));
        assert!(paths.contains(&PathBuf::from("test_files/test_dir/test_dir2/empty_file2")));
    }

    #[test]
    #[should_panic(expected = "Could not find")]
    fn test_parse_bad_path() {
        let path = String::from("idontexist/");
        parse_path(&path).unwrap();
    }
}

