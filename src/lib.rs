/*
    Copyright (c) 2023 Nick Donovan

    This file is part of hecc.

    hecc is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
    License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
    version.

    hecc is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
    details.

    You should have received a copy of the GNU General Public License along with hash_em. If not, see
    <https://www.gnu.org/licenses/>.
 */


pub mod args;
mod hasher;
pub mod paths;

use crate::args::Args;
use crate::hasher::Hasher;

/// Runs the hashing process
///
/// Entry into the hashing library. The options are passed via the Args module which contain
/// algorithm, path, and other selected choices.
///
/// # Arguments
/// * `args` - Reference to the CLI `Args` containing the parameters
///
/// # Examples
/// ```
/// use hecc;
/// use hecc::args::Args;
/// # fn try_run() -> Result<(), String> {
/// let args = Args::build(std::env::args())?;
/// match hecc::run(&args) {
///     Ok(hashes) => {
///             for hash in hashes {
///                 println!("{}", hash);
///             }
///         },
///         Err(e) => {
///             eprintln!("{}", e);
///             std::process::exit(1);
///         }
/// }
/// # Ok(())
/// # }
///
///
pub fn run(args: &Args) -> Result<Vec<String>, String> {
    let mut hasher = Hasher::build(&args.get_algorithm(), args.get_buff_size())?;

    let paths = paths::parse_path(args.get_path())?;

    let mut hashes = Vec::new();
    for path in paths {
        match hasher.hash_file(&path) {
            Ok(hash) => hashes.push(format!("{}  {}", hash, path.display())),
            Err(e) => eprintln!("Error hashing {}: {}", path.display(), e),
        };
    }

    Ok(hashes)
}

#[cfg(test)]
mod tests {
    use crate::args::Args;
    use crate::run;

    #[test]
    fn run_file_test() {
        let a = vec![
            String::from("dummy.exe"),
            String::from("sha1"),
            String::from("./test_files/test.bin"),
        ];
        let args = Args::build(a.into_iter()).unwrap();

        let actual = run(&args).unwrap().get(0).unwrap().to_string();
        let expected = format!("8e1d7e501401059d1c632c0fb09fc37433953297  {}", args.get_path());

        assert_eq!(actual, expected);
    }

    #[test]
    #[should_panic(expected = "Could not find path")]
    fn run_bad_file_test() {
        let a = vec![
            String::from("dummy.exe"),
            String::from("sha1"),
            String::from("./test_files/i_dont_exist.bin"),
        ];
        let args = Args::build(a.into_iter()).unwrap();

        run(&args).unwrap();
    }
}