/*
    Copyright (c) 2023 Nick Donovan

    This file is part of hecc.

    hecc is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
    License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
    version.

    hecc is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
    details.

    You should have received a copy of the GNU General Public License along with hash_em. If not, see
    <https://www.gnu.org/licenses/>.
 */

use hecc;
use hecc::args::Args;

fn main() {
    let args = std::env::args();
    let args = match Args::build(args) {
        Ok(a) => a,
        Err(e) => panic!("{:?}", e),
    };

    match hecc::run(&args) {
        Ok(hashes) => {
            for hash in hashes {
                println!("{}", hash);
            }
        },
        Err(e) => {
            eprintln!("{}", e);
            std::process::exit(1);
        }
    }
}
