# Hash Em Checksum Calculator
Hash Em Checksum Calculator or hecc, calculates the checksum of files, or files in a directory, to help ensure their integrity. 

_This is my initial exploration into the Rust programming language._

## Features
- Ability to hash a single file or a files in a directory.
- Multiple hash algorithms in the same package for ease of use
- Simple to use
- Support for SHA1, SHA256, SHA512, BLAKE2b, and BLAKE2s

## Usage
- `./hecc <algorithm> <path>`
  - `<path>` can be a file or a directory!
  - Example: `./hecc sha256 test.bin`
- `./hecc <path>`
  - If the algorithm is left out, hecc will default to SHA256.
## Roadmap!
- [x] Introduce unit tests
- [x] Add support for command line arguments
- [x] Add support to hash all files in a directory
- [x] Add support for multiple hash types
- [ ] Add ability to verify or check hashes
- [ ] Create documentation
- [ ] Explore multithreading to improve efficiency and speed
- [ ] Add more robust error handling and refactoring (Constantly ongoing)
## Installation
- With Rust installed
  - `git clone https://gitlab.com/nick-donovan/hash_em`
  - `cd hash_em`
  - `cargo build --release`
  - The executable will located at `./target/release/hecc`
- Or use a [release](https://gitlab.com/nick-donovan/hash_em/-/releases)!

## Acknowledgments

This project uses the following repository for calculating checksums:

- [RustCrypto](https://github.com/RustCrypto/hashes) ([Apache License, Version 2.0](https://www.apache.org/licenses/LICENSE-2.0) and [MIT License](https://opensource.org/license/mit/))

### Copying
Hash Em Checksum Calculator is licensed under the GPL version 3 or later. See the [COPYING](https://gitlab.com/nick-donovan/hash_em/-/blob/main/COPYING) file for more information.